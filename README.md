

## Installation

1. Download 
`git clone https://petra_eich@bitbucket.org/petra_eich/playlist.git` 

2. Vendor Files 
If Composer is installed globally, run
```bash
composer update
```

3. Database Credentials 
Rename app_local.example.php to app_local.php.
Resgister Database Credentials.
