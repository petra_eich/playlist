<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * Albums Controller
 *
 * @property \App\Model\Table\AlbumsTable $Albums
 * @method \App\Model\Entity\Album[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AlbumsController extends AppController
{

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->Authorization->skipAuthorization();
        
        $key = $this->request->getQuery('key');
        
        if ( $key ) {
            
            $query = $this->Albums->find('all')->where(['OR' => [
                    ['Albums.name LIKE' => '%' . $key. '%'],
                    ['Artists.name LIKE' => '%' . $key. '%'],
                ],
            ]);
        } else {
            
            $query = $this->Albums;
        }
        
        $this->paginate = [
            'contain' => [
                'Artists',
                'Tracks',
            ],
            'order' => [
                'name' => 'asc',
            ],
        ];
        $albums = $this->paginate($query, [
            'sortableFields' => [
                'name', 
                'Artists.name'
            ]
        ]);

        $tblitems = [
            [
                'name' => 'Titel',
                'field' => [
                    'name' => 'name',
                ],
                'sort' => 'name',
            ],
            [
                'name' => 'Artist',
                'field' => [
                    'name' => 'artist_id',
                    'condition' => 'artist',
                    'type' => 'link',
                    'link' => [
                        'controller' => 'Artists',
                        'action' => 'view',
                        'linktext' => 'name',
                    ]
                ],
                'sort' => 'Artists.name',
            ],
            [
                'name' => 'Actions',
                'class' => 'actions',
                'actionitems' => [
                    [
                        'action' => 'edit',
                        'param' => 'id',
                    ],
                    [
                        'action' => 'view',
                        'param' => 'id',
                    ],
                    [
                        'condition' => ['tracks'],
                        'action' => 'delete',
                        'param' => 'id',
                        'postlink' => __('Are you sure you want to delete?'),
                    ],
                ],
            ],
        ];

        $sidenavitems = [
            ['action' => 'add'],
        ];
        $this->set(compact('sidenavitems', 'albums', 'tblitems'));
    }

    /**
     * View method
     *
     * @param string|null $id Album id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->Authorization->skipAuthorization();
                       
        try {
            
            $album = $this->Albums->get($id, [
                'contain' => ['Artists',],
            ]);
        } catch (RecordNotFoundException $e) {
            
            $this->Flash->info(__('Album do not exist.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
            return $this->redirect(['action' => 'index']);
        }
        
        $items = [
            [
                'label' => 'Titel',
                'text' => $album->name,
                'type'  => 'text',
            ],
            [
                'label'      => 'Artist',
                'type'       => 'text',
                'text'       => $album->artist->name,
                'link'      => ['controller' => 'Artists', 'action' => 'view', $album->artist_id]
            ],
            [
                'label' => 'Bemerkung',
                'field' => $album->comment,
                'type'  => 'textarea',
            ],
        ];

        $query = $this->Albums->Tracks
            ->find()
            ->where(['album_id' => $id]);
        $tracks = $this->paginate($query, ['sortableFields' => ['name']]);

        $tblitems = [];

        if ( count($tracks) > 0 ) {

            $tblitems = [
                [
                    'name' => 'Titel',
                    'field' => [
                        'name' => 'name',
                    ],
                    'sort' => 'name',
                ],
                [
                    'name' => 'Image',
                    'field' => [
                        'name' => 'image',
                        'type' => 'imagelink',
                        'alt' => 'name',
                        'class' => 'img-thumbnail rounded float-start tblpic',
                        'dir' => 'tracks',
                        'link' => [
                            'controller' => 'Tracks',
                            'action' => 'view',
                            'param' => 'id',
                        ]
                    ],
                ],
            ];
        }

        $sidenavitems = [
            ['action' => 'index',],
            [
                'action' => 'edit',
                'param' => $id,
            ],
        ];

        $this->set(compact('sidenavitems', 'album', 'items', 'tracks', 'tblitems'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $album = $this->Albums->newEmptyEntity();
        
        $this->Authorization->authorize($album);

        if ($this->request->is('post')) {

            $album = $this->Albums->patchEntity($album, $this->request->getData());

            if ($this->Albums->save($album)) {

                $this->Flash->info(__('The Album has been saved.'), [
                    'clear' => true,
                    'params' => ['typ' => 'success',],
                ]);
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->info(__('The Album could not be saved. Please, try again.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
        }

        $artists = $this->Albums->Artists->find('list', [
            'keyField' => 'id',
            'valueField' => 'name',
            'limit' => 200,
            'order' => ['name' => 'asc'],
        ]);

        $items = [
            [
                'label' => 'Titel',
                'field' => 'name',
                'type'  => 'text',
            ],
            [
                'label'      => 'Artist',
                'field'      => 'artist_id',
                'type'       => 'select',
                'selectlist' => $artists,
            ],
            [
                'label' => 'Bemerkung',
                'field' => 'comment',
                'type'  => 'textarea',
            ],
        ];

        $sidenavitems = [
            ['action' => 'index'],
        ];
        $this->set(compact('sidenavitems', 'album', 'items'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Album id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {            
        $album = $this->Albums->get($id, [
            'contain' => [
                'Artists',
                'Tracks',
            ],
        ]);
        
        if ( !$album ) {
            
            $this->Flash->info(__('Album do not exist.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
            return $this->redirect(['action' => 'index']);
        } else {
            
            $this->Authorization->authorize($album);
         }


        if ($this->request->is(['patch', 'post', 'put'])) {

            $album = $this->Albums->patchEntity($album, $this->request->getData());

            if ($this->Albums->save($album)) {
                $this->Flash->info(__('The Album {0} has been saved.', $album->name), [
                    'clear' => true,
                    'params' => ['typ' => 'success',],
                ]);

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->info(__('The Album {0} could not be saved. Please, try again.', $album->name), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
        }
        $sidenavitems = [
            ['action' => 'index'],
            [
                'action' => 'view',
                'param' => $id,
            ],
        ];

        if ( empty($album->artist) && empty($album->tracks) ) {

            $sidenavitems[] = [
                'action' => 'delete',
                'param' => $id,
                'postlink' => __('Are you sure you want to delete {0}?', $album->name),
            ];
        }

        $artists = $this->Albums->Artists->find('list', [
            'keyField' => 'id',
            'valueField' => 'name',
            'limit' => 200,
            'order' => ['name' => 'asc'],
        ]);

        $items = [
            [
                'label' => 'Titel',
                'field' => 'name',
                'type'  => 'text',
            ],
            [
                'label'      => 'Artist',
                'field'      => 'artist_id',
                'type'       => 'select',
                'selectlist' => $artists,
            ],
            [
                'label' => 'Bemerkung',
                'field' => 'comment',
                'type'  => 'textarea',
            ],
        ];

        $this->set(compact ('sidenavitems', 'album', 'items'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Album id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $album = $this->Albums->get($id);
        
        $this->Authorization->authorize($album);
        
        if ($this->Albums->delete($album)) {
            
            $message = 'The Album has been deleted.';
            $params = ['typ' => 'success',];
        } else {
            
            $message = 'The Album could not be deleted. Please, try again.';
            $params = ['typ' => 'warning',];

        }
        $this->Flash->info(__($message), [
            'clear' => true,
            'params' => $params,
        ]);
        return $this->redirect(['action' => 'index']);
    }
}
