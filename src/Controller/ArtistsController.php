<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * Artists Controller
 *
 * @property \App\Model\Table\ArtistsTable $Artists
 * @method \App\Model\Entity\Artist[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtistsController extends AppController
{

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->loadModel('Tracks');
        $this->loadModel('Albums');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->Authorization->skipAuthorization();
        
        $key = $this->request->getQuery('key');
        
        if ( $key ) {
            
            $query = $this->Artists->find('all')->where(['OR' => [
                    ['name LIKE' => '%' . $key. '%'],
                ],
            ]);
        } else {
            
            $query = $this->Artists;
        }
        
        $this->paginate = [
            'contain' => [
                'Tracks',
            ],
            'order' => [
                'name' => 'asc',
            ],
        ];
        $artists = $this->paginate($query, [
            'sortableFields' => [
                'name',                    
            ]
        ]);
        
        $tblitems = [
            [
                'name' => 'Name',
                'field' => [
                    'name' => 'name',
                ],
                'sort' => 'name',
            ],
            [
                'name' => 'Actions',
                'class' => 'actions',
                'actionitems' => [
                    [
                        'action' => 'edit',
                        'param' => 'id',
                    ],
                    [
                        'action' => 'view',
                        'param' => 'id',
                    ],
                    [
                        'condition' => 'tracks',
                        'action' => 'delete',
                        'param' => 'id',
                        'postlink' => __('Are you sure you want to delete?'),
                    ],
                ],
            ],
        ];

        $sidenavitems = [
            ['action' => 'add'],
        ];
        $this->set(compact('sidenavitems', 'artists', 'tblitems'));
    }

    /**
     * View method
     *
     * @param string|null $id Artist id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->Authorization->skipAuthorization();
        
        $this->paginate = [
            'order' => ['name' => 'asc',],
        ];
        
        try {
 
            $artist = $this->Artists->get($id);
        } catch (RecordNotFoundException $e) {
            
            $this->Flash->info(__('Artist do not exist.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
            return $this->redirect(['action' => 'index']);
        }
                
        $items = [
            [
                'label' => 'Description',
                'text' => $artist->comment,
                'type'  => 'textarea',
            ],
        ];

        $query = $this->Tracks
            ->find()
            ->contain(['Albums'])
            ->where(['Tracks.artist_id' => $id]);
        $tracks = $this->paginate($query, [
                'sortableFields' => ['name', 'Albums.name'],
                'scope' => 'track',
                'model' => 'Tracks',
            ]);

        $tblitems = [];

        if ( !empty($tracks) ) {

            $tblitems = [
                [
                    'name' => 'Titel',
                    'field' => [
                        'name' => 'name',
                    ],
                    'sort' => 'name',
                ],
                [
                    'name' => 'Image',
                    'field' => [
                        'name' => 'image',
                        'type' => 'imagelink',
                        'alt' => 'name',
                        'class' => 'img-thumbnail rounded float-start tblpic',
                        'dir' => 'tracks',
                        'link' => [
                            'controller' => 'Tracks',
                            'action' => 'view',
                            'param' => 'id',
                        ]
                    ],
                ],
                [
                    'name' => 'Album',
                    'field' => [
                        'name' => 'album_id',
                        'condition' => 'album',
                        'type' => 'link',
                        'link' => [
                            'controller' => 'Albums',
                            'action' => 'view',
                            'linktext' => 'name',
                            'zusatz' => 'trivialname',
                        ]
                    ],
                    'sort' => 'Album.name',
                ],
            ];
        }

        $sidenavitems = [
            ['action' => 'index',],
            [
                'action' => 'edit',
                'param' => $id,
            ],
        ];

        $query = $this->Albums
            ->find()
            ->where(['artist_id' => $id]);
        $albums = $this->paginate($query, [
                'sortableFields' => ['name',],
                'scope' => 'album',
                'model' => 'Albums',
            ]);

        $gentblitems = [];

        if ( !empty($albums) ) {

            $gentblitems = [
                [
                    'name' => 'Titel',
                    'field' => [
                        'name' => 'name',
                    ],
                    'sort' => 'name',
                ],
                [
                    'name' => 'Actions',
                    'class' => 'actions',
                    'actionitems' => [
                        [
                            'controller' => 'Albums',
                            'action' => 'view',
                            'param' => 'id',
                        ],
                    ],
                ],
            ];
        }

        if ( empty($tracks) || empty($albums) ) {

            $sidenavitems[] = [
                'action' => 'delete',
                'param' => $id,
                'postlink' => __('Are you sure you want to delete {0}?', $artist->name),
            ];
        }

        $this->set(compact('sidenavitems','artist', 'tracks', 'items', 'tblitems', 'albums', 'gentblitems'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $artist = $this->Artists->newEmptyEntity();
        
        $this->Authorization->authorize($artist);

        if ($this->request->is('post')) {

            $artist = $this->Artists->patchEntity($artist, $this->request->getData());

            if ($this->Artists->save($artist)) {

                $this->Flash->info(__('The artist has been saved.'), [
                    'clear' => true,
                    'params' => ['typ' => 'success',],
                ]);
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->info(__('The artist could not be saved. Please, try again.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
        }

        $items = [
            [
                'label' => 'Name',
                'field' => 'name',
                'type'  => 'text',
            ],
            [
                'label' => 'Bemerkung',
                'field' => 'comment',
                'type'  => 'textarea',
            ],
        ];

        $sidenavitems = [
            ['action' => 'index'],
        ];
        $this->set(compact('sidenavitems','artist', 'items'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Artist id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $artist = $this->Artists->get($id, [
            'contain' => [
                'Albums',
            ],
        ]);

        $this->Authorization->authorize($artist);
        
        if ($this->request->is(['patch', 'post', 'put'])) {

            $artist = $this->Artists->patchEntity($artist, $this->request->getData());

            if ($this->Artists->save($artist)) {
                $this->Flash->info(__('The artist {0} has been saved.', $artist->name), [
                    'clear' => true,
                    'params' => ['typ' => 'success',],
                ]);

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->info(__('The artist {0} could not be saved. Please, try again.', $artist->name), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
        }
        $sidenavitems = [
            ['action' => 'index'],
            [
                'action' => 'view',
                'param' => $id,
            ],
        ];

        if ( empty($artist->albums) ) {

            $sidenavitems[] = [
                'action' => 'delete',
                'param' => $id,
                'postlink' => __('Are you sure you want to delete {0}?', $artist->name),
            ];
        }

        $items = [
            [
                'label' => 'Name',
                'field' => 'name',
                'type'  => 'text',
            ],
            [
                'label' => 'Bemerkung',
                'field' => 'comment',
                'type'  => 'textarea',
            ],
        ];

        $this->set(compact ('sidenavitems', 'artist', 'items'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Artist id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $artist = $this->Artists->get($id);

        $this->Authorization->authorize($artist);
        
        if ($this->Artists->delete($artist)) {
            
            $message = 'The artist has been deleted.';
            $params = ['typ' => 'success',];
        } else {
            
            $message = 'The artist could not be deleted. Please, try again.';
            $params = ['typ' => 'warning',];

        }
        $this->Flash->info(__($message), [
            'clear' => true,
            'params' => $params,
        ]);
        return $this->redirect(['action' => 'index']);
    }
}
