<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * Tracks Controller
 *
 * @property \App\Model\Table\TracksTable $Tracks
 * @method \App\Model\Entity\Track[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TracksController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Infolinks');
    }

    public function beforeFilter( \Cake\Event\EventInterface $event )
    {
        parent::beforeFilter($event);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->Authorization->skipAuthorization();
        
        $key = $this->request->getQuery('key');
//        exit($key);
        if ( $key ) {
            
            $query = $this->Tracks->find('all')->where(['OR' => [
                    ['Tracks.name LIKE' => '%' . $key. '%'],
                    ['Artists.name LIKE' => '%' . $key. '%'],
                    ['Albums.name LIKE' => '%' . $key. '%'],
                ],
            ]);
        } else {
            
            $query = $this->Tracks;
        }
        $this->paginate = [
            'contain' => [
                'Artists',
                'Albums',
            ],
            'order' => [
                'name' => 'asc',
            ],
        ];
        $tracks = $this->paginate($query, [
            'sortableFields' => [
                'name',  
                'Artists.name', 
                'Albums.name',
            ]
        ]);

        $tblitems = [
            [
                'name' => 'Titel',
                'field' => [
                    'name' => 'name',
                ],
                'sort' => 'name',
            ],
            [
                'name' => 'Image',
                'field' => [
                    'name' => 'image',
                    'type' => 'imagelink',
                    'alt' => 'name',
                    'class' => 'img-thumbnail rounded float-start tblpic',
                    'dir' => 'tracks',
                    'link' => [
                        'controller' => 'Tracks',
                        'action' => 'view',
                        'param' => 'id',
                    ]
                ],
            ],
            [
                'name' => 'Artist',
                'field' => [
                    'name' => 'artist_id',
                    'condition' => 'artist',
                    'type' => 'link',
                    'link' => [
                        'controller' => 'Artists',
                        'action' => 'view',
                        'linktext' => 'name',
                    ]
                ],
                'sort' => 'Artists.name',
            ],
            [
                'name' => 'Album',
                'field' => [
                    'name' => 'album_id',
                    'condition' => 'album',
                    'type' => 'link',
                    'link' => [
                        'controller' => 'Albums',
                        'action' => 'view',
                        'linktext' => 'name',
                    ]
                ],
                'sort' => 'Albums.name',
            ],
            [
                'name' => 'Actions',
                'class' => 'actions',
                'actionitems' => [
                    [
                        'action' => 'edit',
                        'param' => 'id',
                    ],
                    [
                        'action' => 'delete',
                        'param' => 'id',
                        'postlink' => __('Are you sure you want to delete?'),
                    ],
                ],
            ],
        ];

        $sidenavitems = [
            ['action' => 'add'],
        ];
        $this->set(compact('sidenavitems', 'tracks', 'tblitems'));
    }

    /**
     * View method
     *
     * @param string|null $id Track id.
     *
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = NULL)
    {
        $this->Authorization->skipAuthorization();

        try {
            $track = $this->Tracks->get($id, [
                'contain' => [
                    'Artists',
                    'Albums',
                ],
            ]);
        } catch (RecordNotFoundException $e) {
            
            $this->Flash->info(__('Track do not exist.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
            return $this->redirect(['action' => 'index']);
        }
                
        
        $items = [
            [
                'label'      => 'Artist',
                'type'       => 'text',
                'text'       => $track->artist->label,
                'link'      => ['controller' => 'Artists', 'action' => 'view', $track->artist_id]
            ],
            [
                'label'      => 'Album',
                'type'       => 'text',
                'text'       => $track->album->label,
                'link'      => ['controller' => 'Albums', 'action' => 'view', $track->album_id]
            ],
            [
                'label' => 'Description',
                'type'  => 'textarea',
                'text' => $track->comment,
            ],
            [
                'text' => 'tracks/' . $track->image,
                'type'  => 'image',
                'alt' => $track->name,
                'class' => 'img-thumbnail rounded float-start trackpic',
            ],
        ];

        $sidenavitems = [
            [
                'name' => 'List all Tracks',
                'action' => 'index',
            ],
            [
                'action' => 'edit',
                'param' => $id,
            ],
            [
                'action' => 'delete',
                'param' => $id,
                'postlink' => __('Are you sure you want to delete {0}?', $track->name),
            ],
        ];

        $this->set(compact('sidenavitems', 'track', 'items'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $track = $this->Tracks->newEmptyEntity();

        $this->Authorization->authorize($track);
        
        if ( $this->request->is('post') ) {

            $track = $this->Tracks->patchEntity($track, $this->request->getData());

            if ( !$track->getErrors() ) {

                $path = WWW_ROOT . 'img' . DS . 'tracks';
                $image = $this->request->getData('image_file');
                $filename = $image->getClientFilename();


                if ( strlen($filename) > 0 ) {

                    if ( !is_dir($path) ) {
                        mkdir($path, 0755);
                    }
                    $targetPath = $path . DS . $filename;

                    if ( $image ) {
                        $image->moveTo($targetPath);
                    }
                    $track->image = $filename;
                }
            }

//            debug($filename);
//            exit;
            if ( $this->Tracks->save($track) ) {

                $this->Flash->info(__('The track has been saved.'), [
                    'clear'  => TRUE,
                    'params' => ['typ' => 'success',],
                ] );
                return $this->redirect([
                    'action'     => 'index',
                ]);
            } else {

                $this->Flash->info( __('The track could not be saved. Please, try again.'), [
                    'clear'  => TRUE,
                    'params' => ['typ' => 'danger',],
                ] );
            }
        }

        $artists = $this->Tracks->Artists->find('list', [
            'keyField' => 'id',
            'valueField' => 'name',
            'limit' => 200,
            'order' => ['name' => 'asc'],
        ]);
        $albums = $this->Tracks->Albums->find('list', [
            'keyField' => 'id',
            'valueField' => 'name',
            'limit' => 200,
            'order' => ['name' => 'asc'],
        ]);

        $items = [
            [
                'label' => 'Titel',
                'field' => 'name',
                'type'  => 'text',
            ],
            [
                'label' => 'Image',
                'field' => 'image_file',
                'type'  => 'file',
            ],
            [
                'label'      => 'Artist',
                'field'      => 'artist_id',
                'type'       => 'select',
                'selectlist' => $artists,
            ],
            [
                'label'      => 'Album',
                'field'      => 'album_id',
                'type'       => 'select',
                'selectlist' => $albums,
            ],
        ];

        $sidenavitems = [
            [
                'name' => 'List all Tracks',
                'action' => 'index',
            ],
        ];
        $this->set(compact('sidenavitems','track', 'items'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Track id.
     *
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = NULL)
    {
        $track = $this->Tracks->get($id);
        
        $this->Authorization->authorize($track);

        if ( $this->request->is(['patch', 'post', 'put']) ) {

            $track = $this->Tracks->patchEntity($track, $this->request->getData());

            if ( !$track->getErrors() ) {

                $path = WWW_ROOT . 'img' . DS . 'tracks';
                $image = $this->request->getData('image_change');
//              debug($image);
//              exit;
                $filename = $image->getClientFilename();

                if ( strlen($filename) > 0 ) {

                    if ( !is_dir($path) ) {

                        mkdir($path, 0755);
                    }
                    $targetPath = $path . DS . $filename;
                    $image->moveTo($targetPath);

                    if ( isset($track->image) ) {

                        $imagePath = $path . DS . $track->image;

                        if ( file_exists($imagePath) ) {
                            unlink($imagePath);
                        }
                    }
                    $track->image = $filename;
                }
            }

            if ( $this->Tracks->save($track) ) {

                $this->Flash->info( __('The track has been saved.'), [
                    'clear'  => TRUE,
                    'params' => ['typ' => 'success',],
                ] );
                return $this->redirect([
                    'action' => 'edit',
                    $id,
                ]);
            }
            $this->Flash->info( __('The track could not be saved. Please, try again.'), [
                'clear'  => TRUE,
                'params' => ['typ' => 'warning',],
            ] );
        }

        $artists = $this->Tracks->Artists->find('list', [
            'keyField' => 'id',
            'valueField' => 'name',
            'limit' => 200,
            'order' => ['name' => 'asc'],
        ]);
        $albums = $this->Tracks->Albums->find('list', [
            'keyField' => 'id',
            'valueField' => 'name',
            'limit' => 200,
            'order' => ['name' => 'asc'],
        ]);

        $items = [
            [
                'label' => 'Titel',
                'field' => 'name',
                'type'  => 'text',
            ],
            [
                'label' => 'Upload Image',
                'field' => 'image_change',
                'type'  => 'file',
            ],
            [
                'label' => 'Current Image',
                'field' => 'image_current',
                'text' => 'tracks/' . $track->image,
                'type'  => 'image',
                'alt' => $track->name,
                'class' => 'img-thumbnail rounded float-start trackpic',
            ],
            [
                'label'      => 'Artist',
                'field'      => 'artist_id',
                'type'       => 'select',
                'selectlist' => $artists,
            ],
            [
                'label'      => 'Album',
                'field'      => 'album_id',
                'type'       => 'select',
                'selectlist' => $albums,
            ],
            [
                'label' => 'Description',
                'field' => 'comment',
                'type'  => 'textarea',
            ],
        ];

        $sidenavitems = [
            [
                'name' => 'List all Tracks',
                'action' => 'index',
            ],
            [
                'name' => 'Details',
                'action' => 'view',
                'param' => $id,
            ],
            [
                'action' => 'delete',
                'param' => $id,
                'postlink' => __('Are you sure you want to delete {0}?', $track->name),
            ],
        ];

        $this->set(compact('sidenavitems', 'track', 'artists', 'albums', 'items'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Track id.
     *
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = NULL)
    {

        $this->request->allowMethod(['post', 'delete']);
        $track = $this->Tracks->get($id);

        $this->Authorization->authorize($track);
        
        if ( $this->Tracks->delete($track) ) {
            $message = 'The track has been deleted.';
            $params = ['typ' => 'success',];

        } else {
            $message = 'The track could not be deleted. Please, try again.';
            $params = ['typ' => 'warning',];

        }
        $this->Flash->info( __($message), [
            'clear'  => TRUE,
            'params' => $params,
        ] );

        return $this->redirect(['action' => 'index']);
    }

}
