<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Track Entity
 *
 * @property int $id
 * @property int $artist_id
 * @property int $album_id
 * @property string $name
 * @property string $trackno
 * @property string|null $image
 * @property string|null $comment
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 */
class Track extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artist_id' => TRUE,
        'album_id' => TRUE,
        'name' => TRUE,
        'image' => TRUE,
        'trackno' => TRUE,
        'comment' => TRUE,
        'created' => TRUE,
        'modified' => TRUE,

        'album' => TRUE, //belongs to
        'artist' => TRUE, //belongs to
    ];

//    protected function _getLabel()
//    {
//        $trivial = strlen($this->_fields['trivialname']) > 0 ? ' / ' . $this->_fields['trivialname'] : '';
//        return $this->_fields['name'] .  $trivial;
//    }
}
