<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Album;
use Authorization\IdentityInterface;

/**
 * Album policy
 */
class AlbumPolicy
{
    /**
     * Check if $user can add Album
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Album $genus
     * @return bool
     */
    public function canAdd(IdentityInterface $user, Album $album)
    {
        // All logged in users can create
        return true;
    }

    /**
     * Check if $user can edit Album
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Album $genus
     * @return bool
     */
    public function canEdit(IdentityInterface $user, Album $album)
    {
        return $this->isAdmin($user);
    }

    /**
     * Check if $user can delete Album
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Album $genus
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Album $album)
    {
        return $this->isAdmin($user);
    }

    /**
     * Check if $user has role admin
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @return bool
     */
    protected function isAdmin(IdentityInterface $user)
    {
        return $user->get('role') === 'admin';
    }
}
