<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Track;
use Authorization\IdentityInterface;

/**
 * Track policy
 */
class TrackPolicy
{
    /**
     * Check if $user can add Track
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Track $track
     * @return bool
     */
    public function canAdd(IdentityInterface $user, Track $track)
    {
        // All logged in users can create 
        return true;
    }

    /**
     * Check if $user can edit Track
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Track $track
     * @return bool
     */
    public function canEdit(IdentityInterface $user, Track $track)
    {
        return $this->isAdmin($user);
    }

    /**
     * Check if $user can delete Track
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Track $track
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Track $track)
    {
        return $this->isAdmin($user);
    }

    /**
     * Check if $user has role admin
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @return bool
     */
    protected function isAdmin(IdentityInterface $user)
    {
        return $user->get('role') === 'admin';
    }
}
