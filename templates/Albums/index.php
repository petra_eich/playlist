<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Album $album
 * @var $pagecolor
 * @var $tblitems
 */
?>

<?= $this->element('formsearch'); ?>

<div class="card border-<?= $pagecolor; ?> mb-3">

    <?= $this->element('tablecontent',['tbldata' => $albums, 'tblitems' => $tblitems,]); ?>

    <div class="card-footer border-<?= $pagecolor; ?>">
        <small class="text-muted"><?= $this->element('pagination'); ?></small>
    </div>
</div>
