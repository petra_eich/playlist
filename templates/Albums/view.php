<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Album $album
 * @var $pagecolor
 * @var $plantcolor
 * @var $tracks
 * @var $tblitems
 */  ?>

<?= $this->element('formbody', ['formcontent' => $album,]); ?> <?php

if ( count($tracks) > 0 ) { ?>

    <div class="divider py-1 my-4 bg-<?= $pagecolor; ?>"></div>

    <h3 class="text-muted pb-3"><?= __('Related Tracks'); ?></h3>

    <div class="card border-<?= $plantcolor; ?> mb-3">

        <?= $this->element('tablecontent', ['tbldata' => $tracks, 'tblitems' => $tblitems, 'tblcolor' => $plantcolor,]); ?>

        <div class="card-footer border-<?= $plantcolor; ?>">
            <small class="text-muted"><?= $this->element('pagination'); ?></small>
        </div>
    </div> <?php
} ?>
