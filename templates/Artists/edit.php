<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artist $artist
 */ ?>

<?= $this->element('formbody', ['formcontent' => $artist,]); ?>
