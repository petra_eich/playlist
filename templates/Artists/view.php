<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artist $artist
 * @var $pagecolor
 * @var $plantcolor
 * @var $tracks
 * @var $tblitems
 * @var $albums
 * @var $gentblitems
 */
?>

<?= $this->element('formbody', ['formcontent' => $artist]); ?> <?php

if ( count($albums) > 0 ) { ?>

    <div class="divider py-1 my-4 bg-<?= $pagecolor; ?>"></div>

    <h3 class="text-muted pb-3"><?= __('Related Album'); ?></h3>

    <div class="card border-<?= $pagecolor; ?> mb-3">

        <?= $this->element('tablecontent',[
                'tbldata' => $albums,
                'tblitems' => $gentblitems,
                'tblcolor' => $pagecolor,
                'model' => 'Albums',
                'filter' => ['album' => [$artist->id]],
            ]); ?>

        <div class="card-footer border-<?= $pagecolor; ?>">
            <small class="text-muted"><?= $this->element('pagination'); ?></small>
        </div>
    </div> <?php
}

if ( count($tracks) > 0 ) { ?>

    <div class="divider py-1 my-4 bg-<?= $pagecolor; ?>"></div>

    <h3 class="text-muted pb-3"><?= __('Related Tracks'); ?></h3>

    <div class="card border-<?= $plantcolor; ?> mb-3">

        <?= $this->element('tablecontent',[
                'tbldata' => $tracks,
                'tblitems' => $tblitems,
                'tblcolor' => $plantcolor,
                'model' => 'Tracks',
                'filter' => ['track' => [$artist->id]],
        ]); ?>

        <div class="card-footer border-<?= $plantcolor; ?>">
            <small class="text-muted"><?= $this->element('pagination'); ?></small>
        </div>
    </div> <?php
} ?>
