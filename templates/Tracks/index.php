<?php
/**
 * @var \App\Model\Entity\Plant $tracks
 * @var $pagecolor
 * @var $tblitems
 */
?>

<?= $this->element('formsearch'); ?>

<div class="card border-<?= $pagecolor; ?> mb-3">
    <?= $this->element('tablecontent', ['tbldata' => $tracks, 'tblitems' => $tblitems,]); ?>

    <div class="card-footer border-<?= $pagecolor; ?>">
        <small class="text-muted"><?= $this->element('pagination'); ?></small>
    </div>
</div>
