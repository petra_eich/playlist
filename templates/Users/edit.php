<?= $this->Form->create($user) ?>
<?php $id = $user->id; ?>

<div class="card border-warning mb-3">

    <div class="card-header bg-warning text-white">
        <h2><?= __('Edit'); ?></h2>
    </div>

    <div class="card-body text-dark">
        <div class="form-group row">
            <label for="username" class="col-sm-2 col-form-label">Username</label>
            <div class="col-sm-10">
                <?=  $this->Form->control('username', [
                    'type' => 'text',
                    'class' => 'form-control',
                    'id' => 'username',
                    'label' => FALSE,
                ]);?>
            </div>
        </div>
        <div class="form-group row">
            <label for="firstname" class="col-sm-2 col-form-label">Firstname</label>
            <div class="col-sm-10">
                <?=  $this->Form->control('firstname', [
                    'type' => 'text',
                    'class' => 'form-control',
                    'id' => 'firstname',
                    'label' => FALSE,
                ]);?>
            </div>
        </div>
        <div class="form-group row">
            <label for="lastname" class="col-sm-2 col-form-label">Lastname</label>
            <div class="col-sm-10">
                <?=  $this->Form->control('lastname', [
                    'type' => 'text',
                    'class' => 'form-control',
                    'id' => 'lastname',
                    'label' => FALSE,
                ]);?>
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <?=  $this->Form->control('email', [
                    'type' => 'email',
                    'class' => 'form-control',
                    'id' => 'email',
                    'label' => FALSE,
                ]);?>
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
                <?=  $this->Form->control('password', [
                    'type' => 'password',
                    'class' => 'form-control',
                    'id' => 'password',
                    'label' => FALSE,
                ]);?>
            </div>
        </div>
    </div>

    <div class="card-footer bg-transparent">
        <div class="form-group row">
            <div class="col-sm-10">
                <?= $this->Form->button(__('Save'), [
                    'type' => 'submit',
                    'class' => 'btn btn-primary',
                    'id' => 'btnSubmit',
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>

