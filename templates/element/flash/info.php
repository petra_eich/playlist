<?php
/**
 * @var \App\View\AppView $this
 * @var array $params
 * @var string $message
 */

$typ = 'info';
if (!empty($params['typ'])) {
    $typ = $params['typ'];
}

if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
} ?>

<div id="flashmessage" class="alert alert-<?= $typ; ?> alert-dismissible fade show" role="alert">
    <?= $message ?>
<!--    <button type="button" class="close" data-dismiss="alert" aria-label="Close">-->
<!--        <span aria-hidden="true">&times;</span>-->
<!--    </button>-->
</div>
