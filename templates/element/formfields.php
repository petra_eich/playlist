<?php
/**
 * @var $items
 */ ?>

<div class="card-body text-dark"> <?php

    foreach ( $items as $item ) { ?>

        <div class="form-group row"><?php
            if ( $item['type'] == 'button') { ?>

                <label class="col-sm-2 col-form-label"><?= $item['label']; ?></label>

                <div class="col-sm-5">
                    <?= strlen($item['text']) > 0 ? $item['text'] : 'No Data'; ?>
                </div>

                <div class="col-sm-5"> <?php
                    if ( strlen($item['text']) > 0 ) {

                        echo $this->Html->link(__('Edit'),
                            [
                                'controller' => $item['link']['controller'],
                                'action' => 'edit',
                                $item['link']['id'],
                            ],
                            [
                                'class' => 'btn btn-secondary mx-1',
                                'type' => 'button',
                            ]
                        );
                        echo $this->Html->link(__('Details'),
                            [
                                'controller' => $item['link']['controller'],
                                'action' => 'edit',
                                $item['link']['id'],
                            ],
                            [
                                'class' => 'btn btn-primary mx-1',
                                'type' => 'button',
                            ]
                        );
                    } else {

                       echo $this->Html->link( __('Add'),
                            [
                                'controller' => $item['link']['controller'],
                                'action' => 'add',
                                $item['link']['id'],
                            ],
                            [
                                'class' => 'btn btn-success',
                                'type' => 'button',
                            ]
                        );
                    }
                    ?>
                </div> <?php
            } else {

                if ( isset($item['label']) ) { ?>

                    <label for="<?= $item['field']; ?>" class="col-sm-2 col-form-label"><?= $item['label']; ?></label> <?php
                } ?>
                <div class="col-sm-10"> <?php

                    switch ($item['type']) {
                        case 'select':
                            echo $this->Form->select($item['field'], $item['selectlist'], [
                                'class' => 'form-select my-2',
                                'id'    => $item['field'],
                                'label' => FALSE,
                            ]);
                            break;

                        case 'file':
                            echo $this->Form->control($item['field'], [
                                'type'  => $item['type'],
                                'class' => 'form-control my-2',
                                'id'    => $item['field'],
                                'label' => FALSE,
                            ]);
                            break;

                        case 'image':
                            echo $this->Html->image($item['text'], [
                                'alt' => $item['alt'],
                                'class' => $item['class'],
                            ]);
                            break;

                        case 'checkbox': ?>
                                <div class="form-check form-switch my-2"> <?php
                                    echo $this->Form->control($item['field'], [
                                        'type'  => $item['type'],
                                        'class' => 'form-check-input my-2',
                                        'id'    => $item['field'],
                                        'label' => FALSE,
                                    ]); ?>
                                </div> <?php
                            break;

                        case 'hidden':
                            echo $this->Form->control($item['field'], [
                                'type' =>  $item['type'],
                                'value' => $item['val'],
                                'id' => $item['field'],
                            ]);
                            break;

                        default: // text, textarea
                            echo $this->Form->control($item['field'], [
                                'type'  => $item['type'],
                                'class' => 'form-control my-2',
                                'id'    => $item['field'],
                                'label' => FALSE,
                            ]);
                    }  ?>
                </div> <?php
            } ?>
        </div> <?php
    } ?>
</div>
