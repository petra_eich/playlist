<?php
/**
 * @var $controller
 */ ?>

<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="nav navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item<?= $controller == 'Tracks' ? ' active' : '' ?>">
                <h3>
                    <a class="nav-link" href="<?= $this->Url->build(
                        [
                            'controller' => 'Tracks',
                            'action' => 'index',
                        ]); ?>">
                        Tracks
                    </a>
                </h3>
            </li>
        </ul>

        <ul class="nav navbar-nav ms-auto mb-2 mb-lg-0">

            <li class="nav-item<?= $controller == 'Albums' ? ' active' : '' ?>">
                <a class="nav-link" href="<?= $this->Url->build(
                    [
                        'controller' => 'Albums',
                        'action' => 'index',
                    ]); ?>">
                    Album
                </a>
            </li> 
                
            <li class="nav-item<?= $controller == 'Artists' ? ' active' : '' ?>">
                <a class="nav-link" href="<?= $this->Url->build(
                    [
                        'controller' => 'Artists',
                        'action' => 'index',
                    ]); ?>">
                    Artists
                </a>
            </li>
                <?php

            if ( $this->Identity->get('role') == 'admin' ) { ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?= $this->Url->build(
                        [
                            'controller' => 'Users',
                            'action' => 'index',
                        ]); ?>">
                        User
                    </a>
                </li> <?php
            }

            if ( $this->Identity->isLoggedIn() ) { ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?= $this->Url->build(
                        [
                            'controller' => 'Users',
                            'action' => 'logout',
                        ]); ?>">
                        Logout
                    </a>
                </li><?php
            } ?>
        </ul>
    </div>
</nav>

