<?php
/**
 * @var $tblcolor
 * @var $tbldata
 * @var $tblitems
 */
?>

<tbody> <?php
    $actions = [
        'view' => [
            'name' => 'Details',
            'btncolor' => $tblcolor,
        ],
        'edit' => [
            'name' => 'Edit',
            'btncolor' => 'primary',
        ],
        'delete' => [
            'name' => 'Delete',
            'btncolor' => 'danger',
        ],
    ];

    foreach ($tbldata as $data) { ?>

        <tr> <?php
            foreach ( $tblitems as $tblitem ) {

                if ( $tblitem['name'] == 'Actions' ) { ?>

                    <td class="actions"> <?php
                        $actionitems = $tblitem['actionitems'];

                        foreach ( $actionitems as $actionitem ) {

                            $btncolor = isset($actionitem['btncolor']) ? $actionitem['btncolor'] : $actions[$actionitem['action']]['btncolor'];
                            $btnclass = 'mx-1 btn btn-' . $btncolor;
                            $btnname = isset($actionitem['name']) ? $actionitem['name'] : $actions[$actionitem['action']]['name'];
                            $btnurl = [
                                'controller' => isset($actionitem['controller']) ? $actionitem['controller'] : $this->request->getParam('controller'),
                                'action'     => $actionitem['action'],
                                isset($actionitem['param']) ? $data[$actionitem['param']] : NULL,
                            ];

                            if ( array_key_exists('postlink', $actionitem) ) {

                                if ( array_key_exists('condition', $actionitem) ) {

                                    $showbtn = FALSE;

                                    if ( is_array($actionitem['condition']) ) {

                                        $i = 0;
                                        foreach ( $actionitem['condition'] as $condition ) {

                                            if ( !empty($data[$condition]) ) {
                                                $i = $i + 1;
                                            }
                                        }
                                        if ( $i == 0 ) {
                                            $showbtn = TRUE;
                                        }
                                    } else {

                                        if ( empty($data[$actionitem['condition']]) ) {
                                            $showbtn = TRUE;
                                        }
                                    }

                                    if ( $showbtn ) {

                                        echo $this->Form->postLink(
                                            $btnname,
                                            $btnurl,
                                            [
                                                'confirm' => $actionitem['postlink'],
                                                'class'   => $btnclass,
                                                'type'    => 'button',
                                            ]
                                        );
                                    }
                                } else {

                                    echo $this->Form->postLink(
                                        $btnname,
                                        $btnurl,
                                        [
                                            'confirm' => $actionitem['postlink'],
                                            'class'   => $btnclass,
                                            'type'    => 'button',
                                        ]
                                    );
                                }
                            } else {

                                echo $this->Html->link(
                                    $btnname,
                                    $btnurl,
                                    [
                                        'type'  => 'button',
                                        'class' => $btnclass,
                                    ]
                                );
                            }
                        } ?>
                    </td> <?php
                } else { ?>

                    <td> <?php
                        if ( empty($tblitem['field']['type']) ) {

                            echo h($data[$tblitem['field']['name']]);
                        } else {

                            switch ( $tblitem['field']['type'] ) {
                                case 'checkbox': // verwendet in plants/index ?>
                                    <div class="form-check form-switch">
                                        <?= $this->Form->control('active', [
                                            'type'     => 'checkbox',
                                            'class'    => 'form-check-input',
                                            'label'    => FALSE,
                                            'checked'  => $data[$tblitem['field']['name']] ? TRUE : FALSE,
                                            'disabled' => TRUE,
                                        ]); ?>
                                    </div> <?php
                                    break;

                                case 'image': // verwendet in plants/index
                                    if ( strlen($data[$tblitem['field']['name']]) > 0 ) {

                                        $path = isset($tblitem['field']['dir']) ? $tblitem['field']['dir'] . '/' : '';
                                        echo @$this->Html->image($path . $data[$tblitem['field']['name']], [
                                            'alt' => $data[$tblitem['field']['alt']],
                                            'class' => $tblitem['field']['class'],
                                        ]);
                                    }
                                    break;

                                case 'externallink': // verwendet in infolinks/index
                                    echo $this->Html->link(
                                        $data[$tblitem['field']['linktext']],
                                        $data[$tblitem['field']['name']],
                                        [
                                            'escape' => false,
                                            'target' => '_blank',
                                            'title' => $data[$tblitem['field']['linktext']],
                                        ]
                                    );
                                    break;

                                case 'link':
                                    if ( empty($tblitem['field']['condition']) ) {

                                        echo h($data[$tblitem['field']['name']]);
                                    } else {

                                        echo $data->has($tblitem['field']['condition']) ? $this->Html->link($data[$tblitem['field']['condition']][$tblitem['field']['link']['linktext']], ['controller' => $tblitem['field']['link']['controller'], 'action' => $tblitem['field']['link']['action'], $data[$tblitem['field']['name']]]) : '';
                                    }

                                    // siehe plants/index
                                    if ( isset($tblitem['field']['link']['zusatz']) ) {
                                        echo ' - ' . $data[$tblitem['field']['condition']][$tblitem['field']['link']['zusatz']];
                                    }
                                    break;

                                case 'imagelink':  // verwendet in plants/index und infolinks/index
                                    $path = isset($tblitem['field']['dir']) ? $tblitem['field']['dir'] . '/' : '';
                                    $imagefile =  !empty($tblitem['field']['condition']) ? $data[$tblitem['field']['condition']][$tblitem['field']['name']] : $data[$tblitem['field']['name']];
                                    echo $this->Html->link(
                                        $this->Html->image($path . $imagefile,
                                            [
                                                'alt' => !empty($tblitem['field']['condition']) ? $data[$tblitem['field']['condition']][$tblitem['field']['alt']] : $data[$tblitem['field']['alt']],
                                                'class' => $tblitem['field']['class'],
                                            ]),
                                        [
                                           'controller' => $tblitem['field']['link']['controller'],
                                           'action' => $tblitem['field']['link']['action'],
                                            $data[$tblitem['field']['link']['param']],
                                        ],
                                        [
                                            'escapeTitle' => false,
                                            'title' =>  !empty($tblitem['field']['condition']) ? $data[$tblitem['field']['condition']][$tblitem['field']['alt']] : $data[$tblitem['field']['alt']],
                                        ]
                                    );
                                    break;

                                default: // text, textarea
                                    if ( empty($tblitem['field']['contain']) ) {

                                        echo h($data[$tblitem['field']['name']]);
                                    } else {

                                        echo h($data[$tblitem['field']['contain']][$tblitem['field']['name']]);
                                    }
                            }
                        }?>
                    </td> <?php
                }
            } ?>
        </tr> <?php
    } ?>
</tbody>
