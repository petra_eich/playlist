<?php
/**
 * @var $tblcolor
 * @var $headers
 */
?>

<thead class="table-<?= $tblcolor; ?>">
    <tr> <?php
        foreach ($headers as $header) { ?>

            <th>
                <?= $header['name']; ?> <?php
                if ( isset($header['sort']) ) {

                    echo $this->Paginator->sort(
                        $header['sort'],
                        [
                            'asc'  => '<i class="bi bi-sort-up"></i>',
                            'desc' => '<i class="bi bi-sort-down"></i>'
                        ],
                        ['escape' => FALSE]
                    );
                } ?>
            </th> <?php
        } ?>
    </tr>
</thead>
