<?php
/**
 * @var $items
 */ ?>

<div class="card-body text-dark">
   <div class="table-responsive">
        <table class="table table-borderless viewtable">
            <tbody> <?php
                foreach ( $items as $item ) {

                    if ( !empty($item['text']) ) { ?>

                        <tr>
                            <th scope="row"> <?php
                                echo isset($item['label']) ? $item['label'] : ''; ?>
                            </th>
<!--                            <td class="autowidth">-->
                            <td> <?php
                                switch ($item['type']) {
                                    case 'text':
                                        if ( isset($item['link']) ) {
                                            echo  $this->Html->link($item['text'], $item['link']);
                                        } else {
                                            echo($item['text']);
                                        }
                                        break;

                                    case 'textarea':
                                        echo '<blockquote>';
                                        echo $this->Text->autoParagraph(h($item['text']));
                                        echo '</blockquote>';
                                        break;

                                    case 'image':
                                        echo @$this->Html->image($item['text'], [
                                            'alt' => $item['alt'],
                                            'class' => $item['class'],
                                        ]);
                                        break;

                                    case 'checkbox':
                                        echo '<div class="form-check form-switch">';
                                        echo  $this->Form->control($item['label'], [
                                            'type' => 'checkbox',
                                            'class' => 'form-check-input',
                                            'label' => FALSE,
                                            'checked' => $item['text'] ? TRUE : FALSE,
                                            'disabled' => TRUE,
                                        ]);
                                        echo '</div>';
                                        break;

                                    case 'button':
                                        echo ($item['text']);
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->link(__('Details'),
                                            $item['link'],
                                            [
                                                'class' => 'btn btn-secondary',
                                                'type' => 'button',
                                            ]
                                        );
                                        break;

                                    default:
                                        echo($item['text']);
                                } ?>
                            </td>
                        </tr> <?php
                    }
                } ?>
            </tbody>
        </table>
   </div>
</div>
