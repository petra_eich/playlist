<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\old\Habitat $habitats
 * @var $pagecolor
 * @var $tblitems
 */  ?>

<div class="card border-<?= $pagecolor; ?> mb-3">

    <?= $this->element('tablecontent',['tbldata' => $habitats, 'tblitems' => $tblitems,]); ?>

    <div class="card-footer border-<?= $pagecolor; ?>">
        <small class="text-muted"><?= $this->element('pagination'); ?></small>
    </div>
</div>
