<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\old\FeaccessdatasTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FeaccessdatasTable Test Case
 */
class FeaccessdatasTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\old\FeaccessdatasTable
     */
    protected $Feaccessdatas;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Feaccessdatas',
        'app.Projects',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Feaccessdatas') ? [] : ['className' => FeaccessdatasTable::class];
        $this->Feaccessdatas = $this->getTableLocator()->get('Feaccessdatas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Feaccessdatas);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
