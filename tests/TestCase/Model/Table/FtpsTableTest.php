<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\old\FtpsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FtpsTable Test Case
 */
class FtpsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\old\FtpsTable
     */
    protected $Ftps;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Ftps',
        'app.Projects',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Ftps') ? [] : ['className' => FtpsTable::class];
        $this->Ftps = $this->getTableLocator()->get('Ftps', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Ftps);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
