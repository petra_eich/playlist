<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\old\InputdatasTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InputdatasTable Test Case
 */
class InputdatasTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\old\InputdatasTable
     */
    protected $Inputdatas;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Inputdatas',
        'app.Projects',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Inputdatas') ? [] : ['className' => InputdatasTable::class];
        $this->Inputdatas = $this->getTableLocator()->get('Inputdatas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Inputdatas);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
