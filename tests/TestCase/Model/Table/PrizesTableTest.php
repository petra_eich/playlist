<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\old\PrizesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PrizesTable Test Case
 */
class PrizesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\old\PrizesTable
     */
    protected $Prizes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Prizes',
        'app.Projects',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Prizes') ? [] : ['className' => PrizesTable::class];
        $this->Prizes = $this->getTableLocator()->get('Prizes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Prizes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
