<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\old\SslcertsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SslcertsTable Test Case
 */
class SslcertsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\old\SslcertsTable
     */
    protected $Sslcerts;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Sslcerts',
        'app.Projects',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Sslcerts') ? [] : ['className' => SslcertsTable::class];
        $this->Sslcerts = $this->getTableLocator()->get('Sslcerts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Sslcerts);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
