<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\old\TimingsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TimingsTable Test Case
 */
class TimingsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\old\TimingsTable
     */
    protected $Timings;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Timings',
        'app.Projects',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Timings') ? [] : ['className' => TimingsTable::class];
        $this->Timings = $this->getTableLocator()->get('Timings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Timings);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
